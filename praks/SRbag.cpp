#include <iostream>
#include <exception>

using namespace std;


template<typename T>

class Bag {

private:
    int size;

public:
    int p = 0;
    T *arr;

    Bag(T val, int tmp) {
        size = tmp;
        arr = new T[size];
    }

    ~Bag() {
        delete[] arr;
        arr = nullptr;
    }


    T Add(T val2) {
        if (p >= size) {
            throw ("You dont have free space in your bag");
        } else {
            *(arr + p) = val2;
            p++;
            return val2;
        }


    }


    T operator[](int i) {
        if (i >= size) {
            throw ("You dont have this index in your bag");
        } else
            return *(arr + i);
    }
};


int main() {


    Bag<int> bag(5, 3);
    try {
        bag.Add(1);
    }
    catch (char const *a) {
        cout << a << endl;
    }
    try {
        bag.Add(21);
    }
    catch (char const *a) {
        cout << a << endl;
    }
    try {
        bag.Add(31);
    }
    catch (char const *a) {
        cout << a << endl;
    }
    try {
        bag.Add(41);
    }
    catch (char const *a) {
        cout << a << endl;
    }

    try {
        cout << bag[1] << endl;
    }
    catch (char const *a) {
        cout << a << endl;
    }
    try {
        cout << bag[100] << endl;
    }
    catch (char const *a) {
        cout << a << endl;
    }
    return 0;
}
