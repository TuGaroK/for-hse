#include <vector>
#include <string>
#include <random>
#include <iostream>

class Apple{
private:
    int size_;
    int color_;

public:
    Apple() {
        std::random_device rd;
        std::mt19937 gen(rd());
        std::uniform_int_distribution<> size_dis(0, 2);
        std::uniform_int_distribution<> color_dis(0, 2);
        size_ = size_dis(gen);
        color_ = color_dis(gen);
    }

    std::string info()  {
        const std::string sizes[] = { "big", "medium", "small" };
        const std::string colors[] = { "red", "yellow", "green" };
        return sizes[size_] + " " + colors[color_] + " apple";
    }

};

class Tree {

private:
    std::vector<Apple> apples_;

public:
    void add_apple() {
        apples_.emplace_back();
    }

    std::vector<Apple>& get_apples() {
        return apples_;
    }

    void clear_apples() {
        apples_.clear();
    }

};

class Farmer {
private:
    std::vector<Apple> storage_;
public:
    void collect(Tree& tree) {
        storage_.insert(storage_.end(),
                        std::make_move_iterator(tree.get_apples().begin()),
                        std::make_move_iterator(tree.get_apples().end()));
        tree.clear_apples();
    }

    void storage_info() {
        std::cout << "Storage contains " << storage_.size() << " apples:" << std::endl;
        for ( auto& apple : storage_) {
            std::cout << apple.info() << std::endl;
        }
    }


};

int main() {
    Tree tree;
    for (int i = 0; i < 5; ++i) {
        tree.add_apple();
    }
    Farmer farmer;
    farmer.collect(tree);
    farmer.storage_info();
    return 0;
}
