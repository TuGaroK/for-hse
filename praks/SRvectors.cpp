#include <iostream>
#include <fstream>
#include <vector>
#include <initializer_list>
#include <type_traits>

using namespace std;


class Vector3D {
    friend class Vector4D;
private:
    int X;
    int Y;
    int Z;
public:
    Vector3D(initializer_list<int> list) {
        if (list.size() >= 3 && list.size() % 4 != 0) {
            X = *(list.begin());
            Y = *(list.begin() + 1);
            Z = *(list.end() - 1);
        }
    }

    int getX() {
        return X;
    }

    int getY() {
        return Y;
    }

    int getZ() {
        return Z;
    }

    int setX(int tmpx) {
        return X = tmpx;
    }

    int setY(int tmpy) {
        return Y = tmpy;
    }

    int setZ(int tmpz) {
        return Z = tmpz;
    }


    virtual void Info() {
        cout << "Info for Vector3D:" << endl << "X = " << X << " Y = " << Y << " Z = " << Z << endl;
    }
};


class Vector4D : public Vector3D {
private:
    int W;
public:
    Vector4D(initializer_list<int> list) : Vector3D({*(list.begin()), *(list.begin() + 1), *(list.end() - 1)}) {
        if (list.size() % 4 == 0) {
            Z = *(list.end() - 2);
            W = *(list.end() - 1);
        }
    }

    int getW() {
        return W;
    }

    int setW(int tmpw) {
        return W = tmpw;
    }

    void Info() {
        cout << "Info for Vector4D:" << endl << "X = " << getX() << " Y = " << getY() << " Z = " << getZ() << " W = " << W
             << endl;
    }


};

template<class name = Vector4D, int size = 1>
class Manifold {
    static_assert(is_base_of<Vector3D, name>::value,
                  "It is not a derived class");
private:
    int n = 0;
    vector<name> arr;
public:
    void Add(initializer_list<int> list) {

        if (n < size) {
            name v(list);
            arr.push_back(v);
            n++;
        } else {
            cout << " Container is full" << endl;
        }
    }

    void List() {
        for (int i = 0; i < n; i++)
            arr[i].Info();
    }
};


int main() {
    Manifold<Vector4D, 4> v2;
    v2.Add({15, 23, 75, 41});
    v2.Add({56, 71, 45, 23});
    v2.Add({7, 2, 65, 1});
    v2.Add({23, 31, 12, 46});

    v2.List();

        cout<<endl;


    Manifold<Vector3D, 3> v1;
    v1.Add({15, 23, 75});
    v1.Add({56, 71, 45});
    v1.Add({7, 2, 65});
    v1.Add({7, 2, 65}); // Для демонстрации вывода сообщения Container is full


    v1.List();
    return 0;
}
